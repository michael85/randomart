class node {

    constructor() {
        this.child = [];
        this.numChild = 0;
        //this.value;
        //this.maxlevel;
    }

    setNumChild(x) {
        this.numChild = x;
    }

    // setValue(v) {
    //     this.value = v;
    // }

    // setMaxLevel(x) {
    //     this.maxlevel = x;
    // }

    addChild(node) {
        this.child[this.numChild] = node;
        this.numChild++;
    }

    getChild(index) {
        return this.child[index];
    }
}

class xnode extends node {
    evaluate(x, y){
        return x;
    }
    printString(x, y){
        return " Xnode(" + x + ")";
    }
}

class ynode extends node {
    evaluate(x, y){
        return y;
    }
    printString(x, y){
        return " Ynode(" + y + ")";
    }
}

class sinnode extends node {
    evaluate(x, y){
        return Math.sin(Math.PI * this.getChild(0).evaluate(x, y));
    }
    printString(x, y){
        return " SINnode(PI * " + this.getChild(0).printString(x, y) + ")";
    }
}

class cosnode extends node {
    evaluate(x, y){
        return Math.cos(Math.PI * this.getChild(0).evaluate(x, y));
    }
    printString(x, y){
        return " COSnode(PI * " + this.getChild(0).printString(x, y) + ")";
    }
}

class avgnode extends node {
    evaluate(x, y){
        if(this.numChild == 2){
            return ( (this.getChild(0).evaluate(x, y) + this.getChild(1).evaluate(x, y)) / 2);
        }else{
            return ( (this.getChild(0).evaluate(x, y) + this.getChild(1).evaluate(x, y) + 
                this.getChild(2).evaluate(x, y)) / 3);
        }
    }
    printString(x, y){
        if(this.numChild == 2){
            return " AVGnode(" + this.getChild(0).printString(x, y) + " + " + this.getChild(1).printString(x, y) +"/2)";
        }else{
            return " AVGnode(" + this.getChild(0).printString(x, y) + " + " + this.getChild(1).printString(x, y) 
                + this.getChild(2).printString(x, y) + "/3)";
        }
    }
}