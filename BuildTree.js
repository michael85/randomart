function buildTree(MAX_LEVEL) {

    if (MAX_LEVEL == 0) {
        if ((Math.floor((Math.random() * 10) + 1) % 2) == 0) {
            return new xnode();
        } else {
            return new ynode();
        }
    }

    MAX_LEVEL--;
    var n = null;
    switch (Math.floor((Math.random() * 10000) + 1) % 10) {
        //SinNodes
        case 0:
        case 1:
        case 2:
            n = new sinnode();
            n.addChild(buildTree(MAX_LEVEL));
            break;
        //CosNode
        case 3:
        case 4:
        case 5:
            n = new cosnode();
            n.addChild(buildTree(MAX_LEVEL));
            break;
        //AvgNode
        case 6:
            n = new avgnode(2);
            n.addChild(buildTree(MAX_LEVEL));
            n.addChild(buildTree(MAX_LEVEL));
            break;
        case 7:
            n = new avgnode(3);
            n.addChild(buildTree(MAX_LEVEL));
            n.addChild(buildTree(MAX_LEVEL));
            n.addChild(buildTree(MAX_LEVEL));
            break;
        //XNode
        case 8:
            n = new xnode();
            break;
        //YNode
        case 9:
            n = new ynode();
            break;
    }
    return n;
}
