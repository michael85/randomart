
var canvas = document.querySelector('canvas');
var width = canvas.width = 1000;
var height = canvas.height = 750;
var ctx = canvas.getContext('2d');

var MAX_LEVEL = 10;

var r = buildTree(MAX_LEVEL);
var g = buildTree(MAX_LEVEL);
var b = buildTree(MAX_LEVEL);

draw(r, g, b);

//var n = buildTree(MAX_LEVEL);
//console.log(n.printString(3, 2));

//draw();

function randomInteger(min, max) {
    return Math.floor(Math.random() * (+max - +min) + min)
}

function drawPixel(x, y, rgb) {
    ctx.fillStyle = 'rgb(' + rgb[0] + ',' + rgb[1] + ',' + rgb[2] + ')';
    ctx.fillRect(x, y, 1, 1);
}
//sets the color in the (0 - 255) range
function setColor(rgb) {
    rgb[0] = rgb[0] * 255;
    rgb[1] = rgb[1] * 255;
    rgb[2] = rgb[2] * 255;
}

function getRandomColor(rgb) {
    rgb[0] = Math.random();
    rgb[1] = Math.random();
    rgb[2] = Math.random();
}

function draw(r, g, b) {

    var rgb = [0.0, 0.0, 0.0];

    for (var x = 0; x <= width; x++) {
        for (var y = 0; y <= height; y++) {
            //getRandomColor(rgb);

            var i = x / width;
            var j = y / height;
            rgb[0] = r.evaluate(i, j);
            rgb[1] = g.evaluate(i, j);
            rgb[2] = b.evaluate(i, j);

            // console.log(rgb[0]);
            // console.log(rgb[1]);
            // console.log(rgb[2]);

            setColor(rgb);
            drawPixel(x, y, rgb);
        }
    }
}






